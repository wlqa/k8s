### docker常见命令
#### 1. 查看docker基础信息
```
docker info
```
#### 2. 查看docker帮助文档

```
docker --help
```
#### 3. 启动容器
> -d 后台运行
```
docker run -d -it <imageID> /bin/bash 
```
#### 4. 查看前台运行的容器
> 可以加 -a 参数，可以查看所有容器，包括关掉的容器
```
docker ps
```
#### 5. 进入容器

```
docker attach db3 或者 docker attach d48b21a7e439
```
> attach有一个缺点，只要这个连接终止，或者使用了exit命令，容器就会退出后台运行

```
docker exec -it db3 /bin/sh 或者 docker exec -it d48b21a7e439 /bin/sh
```
> exec 不会退出后台，一般用这个命令
#### 6. 关闭启动容器

```
docker stop 容器id
 
docker start 容器id
 
docker restart 容器id
```
#### 7. 提交新的镜像

```
docker commit ecf60a0e0561 streamlit-220313
#可以用下面命令查看提交的新镜像
docker images
```

#### 8. 停止所有容器
> 这样才能够删除其中的images
```
docker stop $(docker ps -a -q)
```
#### 9. 删除所有镜像

```
docker rm $(docker ps -a -q)
```
#### 10. 删除images，通过image的id来指定删除谁

```
docker rmi <image id>
```
> 想要删除untagged images，也就是那些id为<None>的image的话可以用

```
docker rmi $(docker images | grep "^<none>" | awk "{print $3}")
```

#### 11. 删除全部镜像

```
docker rmi $(docker images -q)
```
#### 12. 只显示镜像ID

```
docker images -q
##含中间映像层
docker images -qa 
```
#### 13. 显示镜像信息

```
##显示镜像摘要信息(DIGEST列)
docker images --digests
##显示镜像完整信息
docker images --no-trunc

```
#### 14. 显示镜像历史创建
> 参数：-H 镜像大小和日期，默认为true；--no-trunc  显示完整的提交记录；-q  仅列出提交记录ID
```
docker history -H streamlit
```
#### 15. 搜索镜像

```
docker search mysql
## --filter=stars=600：只显示 starts>=600 的镜像
docker search --filter=stars=600 mysql
## --no-trunc 显示镜像完整 DESCRIPTION 描述
docker search --no-trunc mysql
## --automated ：只列出 AUTOMATED=OK 的镜像
docker search  --automated mysql
```
#### 16. 镜像下载

```
docker pull redis
##下载仓库所有Redis镜像
docker pull -a redis
##下载私人仓库镜像
docker pull bitnami/redis
```
#### 17. 构建镜像

```
##（1）编写dockerfile
cd /docker/dockerfile
vim mycentos
##（2）构建docker镜像
docker build -f /docker/dockerfile/mycentos -t mycentos:1.1
```
#### 18. 查看容器进程

```
docker top redis
##查看所有运行容器的进程信息
for i in  `docker ps |grep Up|awk '{print $1}'`;do echo \ &&docker top $i; done
```
#### 19. 查看容器日志

```
##查看redis容器日志，默认参数
docker logs rabbitmq
##查看redis容器日志，参数：-f  跟踪日志输出；-t   显示时间戳；--tail  仅列出最新N条容器日志；
docker logs -f -t --tail=20 redis
##查看容器redis从2019年05月21日后的最新10条日志。
docker logs --since="2019-05-21" --tail=10 redis

```
#### 20. 容器的进入与退出

```
##使用run方式在创建时进入
docker run -it centos /bin/bash
##关闭容器并退出
exit
##仅退出容器，不关闭
快捷键：Ctrl + P + Q
##直接进入centos 容器启动命令的终端，不会启动新进程，多个attach连接共享容器屏幕，参数：--sig-proxy=false  确保CTRL-D或CTRL-C不会关闭容器
docker attach --sig-proxy=false centos 
##在 centos 容器中打开新的交互模式终端，可以启动新进程，参数：-i  即使没有附加也保持STDIN 打开；-t  分配一个伪终端
docker exec -i -t  centos /bin/bash
##以交互模式在容器中执行命令，结果返回到当前终端屏幕
docker exec -i -t centos ls -l /tmp
##以分离模式在容器中执行命令，程序后台运行，结果不会反馈到当前终端
docker exec -d centos  touch cache.txt 

```
#### 21. 显示最近创建的镜像

```
##显示最近创建容器
docker ps -l
##显示最近创建的3个容器
docker ps -n 3
##不截断输出
docker ps --no-trunc 

```
#### 22. 获取镜像的元信息与IP

```
##获取镜像redis的元信息
docker inspect redis
##获取正在运行的容器redis的 IP
docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' redis

```
#### 23. docker容器和本地之间数据拷贝

```
##将rabbitmq容器中的文件copy至本地路径
docker cp rabbitmq:/[container_path] [local_path]
##将主机文件copy至rabbitmq容器
docker cp [local_path] rabbitmq:/[container_path]/
##将主机文件copy至rabbitmq容器，目录重命名为[container_path]（注意与非重命名copy的区别）
docker cp [local_path] rabbitmq:/[container_path]

```
#### 24. 在容器中执行命令并放入后台

```
docker exec -it ecf60a0e0561 streamlit run /streamlit-python/qianduan.py  &
```
#### 25. 运行一个容器，并挂载本地的路径到容器中

```
docker run --name streamlit -d -p 8501:8501   -v /streamlit-python:/streamlit-python  streamlit
```
























