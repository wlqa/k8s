### prometheus安装

1. #### 下载v9.0版本
```
[root@k8smaster prometheus]# wget -c https://github.com/prometheus-operator/kube-prometheus/archive/v0.9.0.zip
[root@k8smaster prometheus]# ls
v0.10.0.zip
[root@k8smaster prometheus]# unzip v0.9.0.zip 
```
2. #### 查看要下载的镜像
```
[root@k8smaster prometheus]# cd kube-prometheus-0.9.0/
[root@k8smaster kube-prometheus-0.9.0]# find ./manifests/ -type f |xargs grep 'image: '|sort|uniq|awk '{print $3}'|grep ^[a-zA-Z]|grep -Evw 'error|kubeRbacProxy'|sort -rn|uniq

quay.io/prometheus/prometheus:v2.29.1
quay.io/prometheus-operator/prometheus-operator:v0.49.0
quay.io/prometheus/node-exporter:v1.2.2
quay.io/prometheus/blackbox-exporter:v0.19.0
quay.io/prometheus/alertmanager:v0.22.2
quay.io/brancz/kube-rbac-proxy:v0.11.0
k8s.gcr.io/prometheus-adapter/prometheus-adapter:v0.9.0        #这俩网络限制单独下载
k8s.gcr.io/kube-state-metrics/kube-state-metrics:v2.1.1        #
jimmidyson/configmap-reload:v0.5.0
grafana/grafana:8.1.1
```

3. #### 准备镜像（每个master，node节点都操作一遍）
    由于k8s.gcr.io国内访问不了 我们去docker官网https://hub.docker.com/或者通过其他地方下载到每一个容器上

```
[root@k8smaster prometheus]# docker pull zfhub/prometheus-adapter:v0.9.0
[root@k8smaster prometheus]# docker pull zfhub/kube-state-metrics:v2.1.1

给镜像打标签
docker tag zfhub/prometheus-adapter:v0.9.0 k8s.gcr.io/prometheus-adapter/prometheus-adapter:v0.9.0
docker tag zfhub/kube-state-metrics:v2.1.1 k8s.gcr.io/kube-state-metrics/kube-state-metrics:v2.1.1

```
    
4. #### 创建所有服务

```
[root@k8smaster kube-prometheus-0.9.0]# kubectl create -f manifests/setup
[root@k8smaster kube-prometheus-0.9.0]# kubectl create -f manifests/
[root@k8smaster kube-prometheus-0.9.0]# kubectl -n monitoring get all
NAME                                       READY   STATUS    RESTARTS      AGE
pod/alertmanager-main-0                    2/2     Running   2 (99s ago)   39m
pod/alertmanager-main-1                    2/2     Running   2 (89s ago)   39m
pod/alertmanager-main-2                    2/2     Running   2 (98s ago)   39m
pod/blackbox-exporter-6798fb5bb4-mcgfz     3/3     Running   3 (99s ago)   39m
pod/grafana-7476b4c65b-dgskn               1/1     Running   1             39m
pod/kube-state-metrics-74964b6cd4-445xb    3/3     Running   3 (98s ago)   39m
pod/node-exporter-hw2mt                    0/2     Pending   0             19s        #这块pending有问题暂未解决  这边看目前不影响访问
pod/node-exporter-lc5gj                    0/2     Pending   0             19s
pod/node-exporter-p296x                    0/2     Pending   0             19s
pod/prometheus-adapter-5b8db7955f-gdg6l    1/1     Running   1 (98s ago)   39m
pod/prometheus-adapter-5b8db7955f-kdr7n    1/1     Running   1 (98s ago)   39m
pod/prometheus-k8s-0                       2/2     Running   2 (99s ago)   38m
pod/prometheus-k8s-1                       2/2     Running   2 (99s ago)   38m
pod/prometheus-operator-75d9b475d9-wgpg4   2/2     Running   2 (98s ago)   39m

NAME                            TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
service/alertmanager-main       ClusterIP   10.104.51.65     <none>        9093/TCP                     39m
service/alertmanager-operated   ClusterIP   None             <none>        9093/TCP,9094/TCP,9094/UDP   39m
service/blackbox-exporter       ClusterIP   10.96.202.209    <none>        9115/TCP,19115/TCP           39m
service/grafana                 ClusterIP   10.100.89.124    <none>        3000/TCP                     39m
service/kube-state-metrics      ClusterIP   None             <none>        8443/TCP,9443/TCP            39m
service/node-exporter           ClusterIP   None             <none>        9100/TCP                     39m
service/prometheus-adapter      ClusterIP   10.100.240.122   <none>        443/TCP                      39m
service/prometheus-k8s          ClusterIP   10.99.132.157    <none>        9090/TCP                     39m
service/prometheus-operated     ClusterIP   None             <none>        9090/TCP                     38m
service/prometheus-operator     ClusterIP   None             <none>        8443/TCP                     39m

NAME                           DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
daemonset.apps/node-exporter   3         3         0       3            0           kubernetes.io/os=linux   19s

NAME                                  READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/blackbox-exporter     1/1     1            1           39m
deployment.apps/grafana               1/1     1            1           39m
deployment.apps/kube-state-metrics    1/1     1            1           39m
deployment.apps/prometheus-adapter    2/2     2            2           39m
deployment.apps/prometheus-operator   1/1     1            1           39m

NAME                                             DESIRED   CURRENT   READY   AGE
replicaset.apps/blackbox-exporter-6798fb5bb4     1         1         1       39m
replicaset.apps/grafana-7476b4c65b               1         1         1       39m
replicaset.apps/kube-state-metrics-74964b6cd4    1         1         1       39m
replicaset.apps/prometheus-adapter-5b8db7955f    2         2         2       39m
replicaset.apps/prometheus-operator-75d9b475d9   1         1         1       39m

```
5. #### 访问下prometheus的UI
    修改下prometheus UI的service模式，便于我们访问

```
[root@k8smaster kube-prometheus-0.9.0]# kubectl -n monitoring patch svc prometheus-k8s -p '{"spec":{"type":"NodePort"}}'
获取prometheus-k8s对应service的ip：
[root@k8smaster kube-prometheus-0.9.0]#  kubectl get svc prometheus-k8s -n monitoring
NAME             TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
prometheus-k8s   NodePort   10.99.132.157   <none>        9090:32613/TCP   67m
```
    访问http://192.168.168.11（node节点IP）:32613(端口改成自己service对应的端口)：
    点击Status下的Targets可以看到全部资源都处于UP状态，说明成功监控到了：
![prometheus的UI](../../%E6%99%AE%E7%BD%97%E7%B1%B31.png)

    修改grafana服务的类型为nodeport用于前端访问：

```
[root@k8smaster kube-prometheus-0.9.0]#kubectl -n monitoring patch svc grafana -p '{"spec":{"type":"NodePort"}}'

[root@k8smaster kube-prometheus-0.9.0]# kubectl get svc -n monitoring
NAME                    TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
alertmanager-main       ClusterIP   10.104.51.65     <none>        9093/TCP                     71m
alertmanager-operated   ClusterIP   None             <none>        9093/TCP,9094/TCP,9094/UDP   71m
blackbox-exporter       ClusterIP   10.96.202.209    <none>        9115/TCP,19115/TCP           71m
grafana                 NodePort    10.100.89.124    <none>        3000:32001/TCP               71m  #TYPE变了
kube-state-metrics      ClusterIP   None             <none>        8443/TCP,9443/TCP            71m
node-exporter           ClusterIP   None             <none>        9100/TCP                     71m
prometheus-adapter      ClusterIP   10.100.240.122   <none>        443/TCP                      71m
prometheus-k8s          NodePort    10.99.132.157    <none>        9090:32613/TCP               71m
prometheus-operated     ClusterIP   None             <none>        9090/TCP                     71m
prometheus-operator     ClusterIP   None             <none>        8443/TCP                     71m
```
    访问http://192.168.168.11(任意节点IP):32001(端口改成自己service对应的端口)，账号名和密码都是admin：







