```
Service yaml文件详解


apiVersion: v1
kind: Service
metadata:
#元数据
  name: string
  #Service名称
  namespace: string
  #命名空间，不指定时默认为default命名空间
  labels:
  #自定义标签属性列表     
    - name: string
  annotations:
  #自定义注解属性列表    
    - name: string
spec:
#详细描述    
  selector: []
  #Label Selector配置，选择具有指定label标签的pod作为管理范围
  type: string
  #service的类型，指定service的访问方式，默认ClusterIP
  #ClusterIP：虚拟的服务ip地址，用于k8s集群内部的pod访问，在Node上kube-porxy通过设置的iptables规则进行转发
  #NodePort：使用宿主机端口，能够访问各Node的外部客户端通过Node的IP和端口就能访问服务器
  #LoadBalancer：使用外部负载均衡器完成到服务器的负载分发，
  #需要在spec.status.loadBalancer字段指定外部负载均衡服务器的IP，并同时定义nodePort和clusterIP用于公有云环境。
  clusterIP: string
  #虚拟服务IP地址，当type=ClusterIP时，如不指定，则系统会自动进行分配，也可以手动指定。当type=loadBalancer，需要指定
  sessionAffinity: string
  #是否支持session，可选值为ClietIP，默认值为空
  #ClientIP表示将同一个客户端(根据客户端IP地址决定)的访问请求都转发到同一个后端Pod
  ports:
  #service需要暴露的端口列表    
  - name: string
    #端口名称
    protocol: string
    #端口协议，支持TCP或UDP，默认TCP
     port: int
    #服务监听的端口号
     targetPort: int
    #需要转发到后端的端口号
     nodePort: int
    #当type=NodePort时，指定映射到物理机的端口号
  status:
  #当type=LoadBalancer时，设置外部负载均衡的地址，用于公有云环境    
    loadBalancer:
    #外部负载均衡器    
      ingress:
      #外部负载均衡器 
      ip: string
      #外部负载均衡器的IP地址
      hostname: string
     #外部负载均衡器的机主机


```

示例
```
apiVersion: v1             #API的版本号，版本号可以用 kubectl api-versions 查询到
kind: Service              #表明资源对象，例如Pod、RC、Service、Namespace及Node等  
metadata:                  #资源对象的元数据定义
  name: engine             #service名称
spec:                      #资源对象的详细定义，持久化到etcd中保存
  type: ClusterIP          #Service类型，ClusterIP供kubernates集群内部pod访问
  ports:                   #暴露的端口列表
  - port: 8080             #Service监听的端口，对应ClusterIP，即ClusterIP+ServicePort供集群内部pod访问的
    targetPort: 8080       #对应pod中容器的端口
    nodePort: 8080 #port和nodePort都是service的端口，前者暴露给k8s集群内部服务访问，后者暴露给
                   #k8s集群外部流量访问。从上两个端口过来的数据都需要经过反向代理kube-proxy，流
                   #入后端pod的targetPort上，最后到达pod内的容器。
    protocol: TCP          #协议，支持TCP、UDP，默认TCP
    name: http             #端口名称
  selector:                #label选择器，管理label对应的pod
    name: enginehttpmanage #pod的label


```
