
//Pod yaml文件详解
```
apiVersion: v1			#必选，版本号，例如v1
kind: Pod				#必选，Pod
metadata:				#必选，元数据
  name: string			  #必选，Pod名称
  namespace: string		  #必选，Pod所属的命名空间
  labels:				  #自定义标签
    - name: string		    #自定义标签名字
  annotations:			    #自定义注释列表
    - name: string
spec:					#必选，Pod中容器的详细定义
  containers:			  #必选，Pod中容器列表
  - name: string		    #必选，容器名称
    image: string		    #必选，容器的镜像名称
    imagePullPolicy: [Always | Never | IfNotPresent]	#获取镜像的策略：Alawys表示总是下载镜像，IfnotPresent表示优先使用本地镜像，否则下载镜像，Nerver表示仅使用本地镜像
    command: [string]		#容器的启动命令列表，如不指定，使用打包时使用的启动命令
    args: [string]			#容器的启动命令参数列表
    workingDir: string		#容器的工作目录
    volumeMounts:			#挂载到容器内部的存储卷配置
    - name: string			  #引用pod定义的共享存储卷的名称，需用volumes[]部分定义的的卷名
      mountPath: string		  #存储卷在容器内mount的绝对路径，应少于512字符
      readOnly: boolean		  #是否为只读模式
    ports:					#需要暴露的端口库号列表
    - name: string			  #端口号名称
      containerPort: int	  #容器需要监听的端口号
      hostPort: int			  #容器所在主机需要监听的端口号，默认与Container相同
      protocol: string		  #端口协议，支持TCP和UDP，默认TCP
    env:					#容器运行前需设置的环境变量列表
    - name: string			  #环境变量名称
      value: string			  #环境变量的值
    resources:				#资源限制和请求的设置
      limits:				  #资源限制的设置
        cpu: string			    #Cpu的限制，单位为core数，将用于docker run --cpu-shares参数
        memory: string			#内存限制，单位可以为Mib/Gib，将用于docker run --memory参数
      requests:				  #资源请求的设置
        cpu: string			    #Cpu请求，容器启动的初始可用数量
        memory: string		    #内存清楚，容器启动的初始可用数量
    livenessProbe:     		#对Pod内个容器健康检查的设置，当探测无响应几次后将自动重启该容器，检查方法有exec、httpGet和tcpSocket，对一个容器只需设置其中一种方法即可
      exec:					#对Pod容器内检查方式设置为exec方式
        command: [string]	  #exec方式需要制定的命令或脚本
      httpGet:				#对Pod内个容器健康检查方法设置为HttpGet，需要制定Path、port
        path: string
        port: number
        host: string
        scheme: string
        HttpHeaders:
        - name: string
          value: string
      tcpSocket:			#对Pod内个容器健康检查方式设置为tcpSocket方式
         port: number
       initialDelaySeconds: 0	#容器启动完成后首次探测的时间，单位为秒
       timeoutSeconds: 0		#对容器健康检查探测等待响应的超时时间，单位秒，默认1秒
       periodSeconds: 0			#对容器监控检查的定期探测时间设置，单位秒，默认10秒一次
       successThreshold: 0
       failureThreshold: 0
       securityContext:
         privileged:false
    restartPolicy: [Always | Never | OnFailure]		#Pod的重启策略，Always表示一旦不管以何种方式终止运行，kubelet都将重启，OnFailure表示只有Pod以非0退出码退出才重启，Nerver表示不再重启该Pod
    nodeSelector: obeject		#设置NodeSelector表示将该Pod调度到包含这个label的node上，以key：value的格式指定
    imagePullSecrets:			#Pull镜像时使用的secret名称，以key：secretkey格式指定
    - name: string
    hostNetwork:false			#是否使用主机网络模式，默认为false，如果设置为true，表示使用宿主机网络
    volumes:					#在该pod上定义共享存储卷列表
    - name: string				  #共享存储卷名称 （volumes类型有很多种）
      emptyDir: {}				  #类型为emtyDir的存储卷，与Pod同生命周期的一个临时目录。为空值
      hostPath: string			  #类型为hostPath的存储卷，表示挂载Pod所在宿主机的目录
        path: string			    #Pod所在宿主机的目录，将被用于同期中mount的目录
      secret:					#类型为secret的存储卷，挂载集群与定义的secre对象到容器内部
        scretname: string  
        items:     
        - key: string
          path: string
      configMap:				#类型为configMap的存储卷，挂载预定义的configMap对象到容器内部
        name: string
        items:
        - key: string
```

示例
```
apiVersion：v1  #必选，指定K8s部署的API的版本号
kind:Pod   #必选，资源类型Pod
metadata: # 必选，记录部署应用的元数据
  name: nginx # 必选，符合 RFC 1035 规范的 Pod 名称
  namespace: web-testing # 可选，不指定默认为 default，Pod 所在的命名空间
  labels: # 可选，标签选择器，一般用于 Selector
    - app: nginx
  annotations: # 可选，注释列表
    - app: nginx
spec: # 必选，用于部署容器的详细信息
  containers: # 必选，容器列表
  - name: nginx # 必选，符合 RFC 1035 规范的容器名称
    image: nginx:v1 # 必选，容器所用的镜像的地址
    imagePullPolicy: Always # 可选，镜像拉取策略
    workingDir: /usr/share/nginx/html # 可选，容器的工作目录
    volumeMounts: # 可选，存储卷配置（容器内部挂载）
    - name: webroot # 存储卷名称
      mountPath: /usr/share/nginx/html # 挂载目录
      readOnly: true # 只读
    ports: # 可选，容器需要暴露的端口号列表
    - name: http # 端口名称
      containerPort: 80 # 端口号
      protocol: TCP # 端口协议，默认 TCP
    env: # 可选，环境变量配置
    - name: TZ # 变量名
      value: Asia/Shanghai
    - name: LANG
      value: en_US.utf8
    resources: # 可选，资源限制和资源请求限制
      limits: # 最大限制设置
        cpu: 1000m
        memory: 1024MiB
      requests: # 启动所需的资源
        cpu: 100m
        memory: 512MiB
     readinessProbe: # 可选，容器状态检查
       httpGet: # 检测方式
         path: / # 检查路径
         port: 80 # 监控端口
       timeoutSeconds: 2 # 超时时间
       initialDelaySeconds: 60 # 初始化时间
     livenessProbe: # 可选，监控状态检查
       exec: # 检测方式
         command:
         - cat
         - /health
       httpGet: # 检测方式
         path: /_health
         port: 8080
         httpHeaders:
         - name: end-user
           value: jason
        tcpSocket: # 检测方式
          port: 80
        initialDelaySeconds: 60 # 初始化时间
        timeoutSeconds: 2 # 超时时间
        periodSeconds: 5 # 检测间隔
        successThreshold: 2 # 检查成功为 2 次表示就绪
        failureThreshold: 1 # 检测失败 1 次表示未就绪
      securityContext: # 可选，限制容器不可信的行为
        provoleged: false
     restartPolicy: Always # 可选，默认为 Always
     nodeSelector: # 可选，指定 Node 节点
       region: subnet7
     imagePullSecrets: # 可选，拉取镜像使用的 secret
     - name: default-dockercfg-86258
     hostNetwork: false # 可选，是否为主机模式，如是，会占用主机端口
     volumes: # 共享存储卷列表（外部共享）
     - name: webroot # 名称，与上述对应
       emptyDir: {} # 共享卷类型，空
       hostPath: # 共享卷类型，本机目录
         path: /etc/hosts
       secret: # 共享卷类型，secret 模式，一般用于密码
         secretName: default-token-tf2jp # 名称
         defaultMode: 420 # 权限
         configMap: # 一般用于配置文件
         name: nginx-conf
         defaultMode: 420
         
[root@k8s-m-01 ~]# kubectl explain pod.spec  #查看参数
 string : 跟字符串
Object ：
[]Object : 
	- name

```