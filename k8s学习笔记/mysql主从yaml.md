### k8s部署mysql主从

#### 1. 创建数据目录
创建nfs目录以便共享给mysql pod
```
mkdir /data/k8s/
chmod 777 k8s
cd k8s
mkdir mysql
cd mysql 
mkdir master
mkdir slave
vi /etc/exports
/data/k8s/ *(insecure,rw,sync,no_root_squash)
systemctl restart nfs

```
然后挂载到k8s节点上

#### 2. 配置mysql密码
配置root的mysql密码123

```
echo -n '123' | base64

[root@master mysql-zc]# cat mysql-secret.yaml 
apiVersion: v1
data:
  MYSQL_ROOT_PASSWORD: MTIz
kind: Secret
metadata:
  name: mysql-secret
  namespace: default
type: Opaque  #不透明

```
#### 3. 编写my.cnf配置文件
master my.cnf配置

```
[root@master mysql-zc]# cat master-cnf.yaml 
apiVersion: v1
kind: ConfigMap
metadata:
  name: mysql-master-cnf
  namespace: default
data:
  my.cnf: |-    #多行文件内容用 ｜- 
    [client]
    default-character-set=utf8
    [mysql]
    default-character-set=utf8
    [mysqld]
    init_connect='SET collation_connection = utf8_unicode_ci'
    init_connect='SET NAMES utf8'
    character-set-server=utf8
    collation-server=utf8_unicode_ci
    skip-character-set-client-handshake
    skip-name-resolve
    server_id=1
    log-bin=mysql-bin
    read-only=0
    #binlog-do-db=admin 需要主从复制的表
    replicate-ignore-db=mysql
    replicate-ignore-db=sys
    replicate-ignore-db=information_schema
    replicate-ignore-db=performance_schema
```
slave my.cnf配置

```
[root@master mysql-zc]# cat slave-cnf.yaml 
apiVersion: v1
kind: ConfigMap
metadata:
  name: mysql-slave-cnf
  namespace: default
data:
  my.cnf: |-
    [client]
    default-character-set=utf8
    [mysql]
    default-character-set=utf8
    [mysqld]
    init_connect='SET collation_connection = utf8_unicode_ci'
    init_connect='SET NAMES utf8'
    character-set-server=utf8
    collation-server=utf8_unicode_ci
    skip-character-set-client-handshake
    skip-name-resolve
    server_id=2
    log-bin=mysql-bin
    read-only=1
    replicate-ignore-db=mysql
    replicate-ignore-db=sys
    replicate-ignore-db=information_schema
    replicate-ignore-db=performance_schema
```
#### 4. 配置pv
master挂载卷配置

```
[root@master mysql-zc]# cat master-pv.yaml 
apiVersion: v1
kind: PersistentVolume
metadata:
  name: mysql-pv-master
spec:
  accessModes:
    - ReadWriteOnce
  capacity:
    storage: 1Gi
  nfs:
    path: /data/k8s/mysql/master  #nsf要挂载到容器内的本地路径
    readOnly: false  #读写权限
    server: 10.0.64.200
```
slave挂载卷配置

```
[root@master mysql-zc]# cat slave-pv.yaml 
apiVersion: v1
kind: PersistentVolume
metadata:
  name: mysql-pv-slave
spec:
  accessModes:
    - ReadWriteOnce
  capacity:
    storage: 1Gi
  nfs:
    path: /data/k8s/mysql/slave
    readOnly: false
    server: 10.0.64.200
```
#### 5. 配置pvc
master pvc配置
```
[root@master mysql-zc]# cat master-pvc.yaml 
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-pvc-master
  namespace: default
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  volumeName: mysql-pv-master  #需要挂载的pv名称，对应上边的pv
```
slave pvc配置：

```
[root@master mysql-zc]# cat slave-pvc.yaml 
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-pvc-slave
  namespace: default
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  volumeName: mysql-pv-slave
```
#### 6. 创建有状态副本集 StatefulSet
master有状态副本集

```
[root@master mysql-zc]# cat master-mysql.yaml 
apiVersion: apps/v1
kind: StatefulSet  #有序的pod
metadata:
  namespace: default
  labels:
    app: mysql-master
  name: mysql-master
  annotations:
    kubesphere.io/alias-name: mysql主节点
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mysql-master
  template:
    metadata:
      labels:
        app: mysql-master
      annotations:
       # kubesphere.io/containerSecrets: null
        logging.kubesphere.io/logsidecar-config: '{}'
    spec:
      containers:
        - name: master-container
         # type: worker 
          imagePullPolicy: IfNotPresent  #本地有镜像就不下载
          resources:
            requests:  #资源限制最小
              cpu: '0.01'
              memory: 10Mi
            limits:  #资源最大限制
              cpu: '0.98'
              memory: 1700Mi
          image: 'mysql:5.7'
          ports:
            - name: tcp-3306
              protocol: TCP
              containerPort: 3306  #pod 暴漏的端口数
            - name: tcp-33060
              protocol: TCP
              containerPort: 33060
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mysql-secret  #变量来自mysql-secret
                  key: MYSQL_ROOT_PASSWORD
          volumeMounts:      #容器内挂载点
            - name: master-cnf-volume
              readOnly: false            #读写权限
              mountPath: /etc/mysql    #要把挂载点挂载到这个位置
            - name: master-data-volume
              readOnly: false
              mountPath: /var/lib/mysql
      serviceAccount: default
      affinity:
        podAntiAffinity:        #节点亲和力
          preferredDuringSchedulingIgnoredDuringExecution:    #软策略 如果你没有满足调度要求的节点的话，POD 就会忽略这条规则，继续完成调度过程
            - weight: 100                                        ##说白了就是满足条件最好了，没有的话也无所谓了的策略
              podAffinityTerm:
                labelSelector:
                  matchLabels:
                    app: mysql-master
                topologyKey: kubernetes.io/hostname
      initContainers: []
      imagePullSecrets: null
      volumes:
        - name: master-cnf-volume     #映射configMap信息
          configMap:
            name: mysql-master-cnf
            items:
              - key: my.cnf
                path: my.cnf
        - name: master-data-volume    #映射pvc信息
          persistentVolumeClaim:
            claimName: mysql-pvc-master
  updateStrategy:
    type: RollingUpdate
    rollingUpdate:
      partition: 0
  serviceName: mysql-master
```

slave有状态副本集

```
[root@master mysql-zc]# cat slave-mysql.yaml 
apiVersion: apps/v1
kind: StatefulSet
metadata:
  namespace: default
  labels:
    app: mysql-slave
  name: mysql-slave
  annotations:
    kubesphere.io/alias-name: mysql主节点
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mysql-slave
  template:
    metadata:
      labels:
        app: mysql-slave
      annotations:
       # kubesphere.io/containerSecrets: null
        logging.kubesphere.io/logsidecar-config: '{}'
    spec:
      containers:
        - name: slave-container
         # type: worker
          imagePullPolicy: IfNotPresent
          resources:
            requests:
              cpu: '0.01'
              memory: 10Mi
            limits:
              cpu: '0.98'
              memory: 1700Mi
          image: 'mysql:5.7'
          ports:
            - name: tcp-3306
              protocol: TCP
              containerPort: 3306
             # servicePort: 3306
            - name: tcp-33060
              protocol: TCP
              containerPort: 33060
             # servicePort: 33060
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mysql-secret
                  key: MYSQL_ROOT_PASSWORD
          volumeMounts:
            - name: slave-cnf-volume
              readOnly: false
              mountPath: /etc/mysql
            - name: slave-data-volume
              readOnly: false
              mountPath: /var/lib/mysql
      serviceAccount: default
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 100
              podAffinityTerm:
                labelSelector:
                  matchLabels:
                    app: mysql-slave
                topologyKey: kubernetes.io/hostname
      initContainers: []
      imagePullSecrets: null
      volumes:
        - name: slave-cnf-volume     #映射configMap信息
          configMap:
            name: mysql-slave-cnf
            items:
              - key: my.cnf
                path: my.cnf
        - name: slave-data-volume    #映射pvc信息
          persistentVolumeClaim:
            claimName: mysql-pvc-slave
  updateStrategy:
    type: RollingUpdate
    rollingUpdate:
      partition: 0
  serviceName: mysql-slave


```
#### 7. 创建无头服务Headless
master 无头服务

```
[root@master mysql-zc]# cat master-hs.yaml 
apiVersion: v1
kind: Service
metadata:
  namespace: default
  labels:
    app: mysql-master
  annotations:
    kubesphere.io/serviceType: statefulservice
    kubesphere.io/alias-name: mysql主节点
  name: mysql-master
spec:
  sessionAffinity: ClientIP
  selector:
    app: mysql-master
  ports:
    - name: tcp-3306
      protocol: TCP
      port: 3306
      targetPort: 3306
    - name: tcp-33060
      protocol: TCP
      port: 33060
      targetPort: 33060
  clusterIP: None
  sessionAffinityConfig:
    clientIP:
      timeoutSeconds: 10800

```
slave无头服务


```
[root@master mysql-zc]# cat slave-hs.yaml 
apiVersion: v1
kind: Service
metadata:
  namespace: default
  labels:
    app: mysql-slave
  annotations:
    kubesphere.io/serviceType: statefulservice
    kubesphere.io/alias-name: mysql主节点
  name: mysql-slave
spec:
  sessionAffinity: ClientIP
  selector:
    app: mysql-slave
  ports:
    - name: tcp-3306
      protocol: TCP
      port: 3306
      targetPort: 3306
    - name: tcp-33060
      protocol: TCP
      port: 33060
      targetPort: 33060
  clusterIP: None
  sessionAffinityConfig:
    clientIP:
      timeoutSeconds: 10800
```

#### 8. 创建外部访问NodePort
master NodePort service

```
[root@master mysql-zc]# cat master-svc.yaml 
apiVersion: v1
kind: Service
metadata:
  name: mysql-master-front
  labels:
    app: mysql-master
  namespace: default
spec:
  selector:
    app: mysql-master
  type: NodePort
  ports:
    - name: ''
      port: 3306
      protocol: TCP
      targetPort: 3306
      nodePort: 30001  #指定主机任意端口30000-32767
  sessionAffinity: None

```
slave NodePort Service

```
[root@master mysql-zc]# cat slave-svc.yaml 
apiVersion: v1
kind: Service
metadata:
  name: mysql-slave-front
  labels:
    app: mysql-slave
  namespace: default
spec:
  selector:
    app: mysql-slave
  type: NodePort
  ports:
    - name: ''
      port: 3306
      protocol: TCP
      targetPort: 3306
      nodePort: 30002    #指定主机任意端口30000-32767
  sessionAffinity: None

```
执行kubectl apply -f *.yaml 创建pod。
此时，mysql主机的部署已经完成，可以使用外部客户端访问了。

#### 9. 主从同步
进入mysql-master容器内部

```
# 1.进入mysql内部
[root@master ~]# kubectl exec -it service/mysql-master /bin/bash
#在/etc/hosts目录中添加主从两台的IP以便解析 echo输入
>  mysql -uroot -p123
#切换到 mysql DB
mysql> USE mysql;   
# 查看root用户是否具备远程访问权限
mysql> select Host,User,authentication_string,password_expired,password_last_changed from user; 

# 2.授权 root可以远程访问（主从无关，如root没有访问权限，执行以下命令，方便我们远程连接MySQL）
mysql> grant all privileges on *.* to 'root'@'%' identified by 'root' with grant option;
Query OK, 0 rows affected, 1 warning (0.00 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.00 sec)

# 3.添加用来同步的用户
mysql> GRANT REPLICATION SLAVE ON *.* to 'backup'@'%' identified by '123456';
Query OK, 0 rows affected, 1 warning (0.01 sec)

# 4.查看master状态
mysql> show master status\G
*************************** 1. row ***************************
             File: mysql-bin.000003
         Position: 742
     Binlog_Do_DB: 
 Binlog_Ignore_DB: 
Executed_Gtid_Set: 
1 row in set (0.00 sec)
```

进入mysql-slave内部

```
# 进入mysql内部
mysql -uroot -p123

# 设置主库连接  主库 dns: mysql-master.default.svc.cluster.local 
change master to master_host='mysql-master.default.svc.cluster.local',master_user='backup',master_password='123456',master_log_file='mysql_bin.000003',master_log_pos=0,master_port=3306;
##master_log_pos=0 的值啥mysql master上的 Position: 742 的值


# 启动从库同步
start slave;

# 查看从从库状态
show slave status\G;

             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
#当状态为yes为主从设置成功

#如果不成功可以执行下面
stop slave;
reset slave;
start slave;
```

可以在master上创建新的库slave上也能看到了







 