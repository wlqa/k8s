### #k8s相关

#### kubernetes的组件有哪些？

    1.master节点： 
        kube-apiServer：所有资源统一的增删改查入口，只有apiserver能与etcd交互

        kube-scheduler：负责集群的资源调度，接收来自kube-apiserver创建pods的任务，收到任务后它会检索出所有符合该pod要求的node节点（通过预选策略和优选策略），开始执行pod调度逻辑。调度成功后将Pod绑定到目标节点上。

        kube-contorller-manager：所有容器的自动化控制，负责集群内Node，Namespace，Service，Token，Replication等资源对象的管理，使集群内的资源对象维持在预期的工作状态。当发生故障，导致资源对象的工作状态发生变化，就进行干预，尝试将资源对象从当前状态恢复到于其的工作状态。

        etcd：存储任务状态，新增节点，pod宕掉等，k8s中的每个组件只需要监听etcd中数据，就可以知道自己应该做什么   

    2.node 节点：
        kubeproxy：实现k8s server的通信与负载均衡重要组件

        kubelet：通过api-server检测到kube-scheduler分配pod节点信息，然后从etcd那里获取清单，并下载pod

        container_runtime

#### kubernetes的网络插件有哪些？各种网络插件的优缺点是什么。

    1. Flanel    容易安装配置，是打包好的二进制文件，不需要专门的数据库存储信息，可以用etcd

    2. calico    性能灵活性比flanel好，不仅提供网络连接还提供网络安全管理，

    3. weave    Weave在集群中的每个节点之间创建网状Overlay网络

#### kubernetes的两个容器之间无法通信你觉得是什么原因？
    1. 被namespace逻辑隔离

    2. pod状态异常

#### 简单说一下kubernetes的service？service底层如何实现。
    1. 什么是server
     service主要用于负载均衡和服务发现，pod是具有生命周期的，如果pod重启或他的IP地址发生变化，但是应用服务把IP写死这个时候就会找不到相对应的pod，这个时候可以通过service进行统一入口访问。

    2. service的底层实现。
    service只是把pod关联起来，当创建serive时候会创建endpoint对象，endpoint是用来做容器发现的，路由转发由kubeproxy实现的当用户创建 service 后 endpoints controller 会监听 pod 的状态，当 pod 处于 running 且准备就绪时，endpoints controller 会将 pod ip 记录到 endpoints 对象中，因此，service 的容器发现是通过 endpoints 来实现的。

#### 简单说一下容器的生命周期？
    1. init  初始化容器  Init容器总是运行到成功完成为止 每个Init容器都必须在下一个Init容器启动之前成功完成

    2. 容器探针  存活性探针 就绪探针  检测容器状态

    3. 容器终止
       参考链接：https://blog.csdn.net/m0_46656833/article/details/120406556?spm=1001.2101.3001.6650.5&utm_medium=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromBaidu~Rate-5.queryctrv4&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromBaidu~Rate-5.queryctrv4&utm_relevant_index=10

#### 简单说一下kubernetes的调度策略？
     调度策略可以根据标签，亲和度，污点，节点资源限制决定分配到哪个节点上。

#### 搭建kubernetes集群过程中遇到了什么问题？
    1. kubeadmin集群 master节点加入不了

    2. 如果发现启动kubelet.service卡住，光标闪烁而没有反应 需要登录master颁发证书

    3. 拉取镜像失败
          查看镜像源是否更改默认是国外，由于政策限制，可以更换国内源

    4. 加入集群后节点状态一直是NotReady
        出现这个错误可以在节点机器上执行journalctl -f -u kubelet查看kubelet的输出日志信息.
        如果是报错网络问题，查看网络插件有没有准备好.我们可以执行命令docker images|grep flannel来查看flannel镜像是否已经成功拉取下来.
            经过排查,flannel镜像拉取的有点慢,稍等一会以后就ok了.

    5. 节点加入集群时报错
        节点加入集群时,可能会报各种各样的错误,有些容易解决,有些可能一时无法找到好的解决办法.这时候可以尝试使用kubeadm reset命令来重置一下,然后再重新加入.

#### flennel网络插件如何打通两个Pod容器之间的通信？
     flannel是一个网络组件，可以为不同node节点的分配不同的子网，实现容器间的跨机通信，从而实现整个kubenets层级通信。 由此可知，该组件运行于node节点上，依赖组件为etcd，docker。

     flannel在node上分配大子网然后在给node上每个pod分配小子网并且唯一，这样在大子网下的pod之间互相通信。

#### iptables为什么会转向ipvs
     从k8s的1.8版本开始，kube-proxy引入了IPVS模式，IPVS模式与iptables同样基于Netfilter，但是采用的hash表，因此当service数量达到一定规模时，hash查表的速度优势就会显现出来，从而提高service的服务性能

     链接：https://www.jianshu.com/p/9b4b700c7765

#### 我这边有一个节点的pod出现异常情况，一个节点ping另一台机器ping不通，telnet不通是什么原因。
     有可能一直重启状态

#### k8s的网络通信模式
    1. 同一pod内： 在同一个namespace，共享一个linux协议栈

    2. pod1到pod2： 
          不在同一台主机：将pod的ip和node的ip关联起来，通过这个关联让pod互相访问
          在同一台主机： 由docker0网桥直接将请求转发到pod2

    3. pod到service： 由IPtable转发和维护

    4. pod到外网： pod向外发出请求，查找路由表，转发数据到物理网卡，物理网卡完成选择后由IPtable执行，把源ip替换成物理网卡IP，向外发出请求。

#### 说一下ingress的工作原理
    1. ingress对象：
        指的是k8s中的一个api对象，一般用yaml配置。作用是定义请求如何转发到service的规则，可以理解为配置模板。

    2. ingress-controller：
        具体实现反向代理及负载均衡的程序，对ingress定义的规则进行解析，根据配置的规则来实现请求转发。
        简单来说，ingress-controller才是负责具体转发的组件，通过各种方式将它暴露在集群入口，外部对集群的请求流量会先到ingress-controller，
        而ingress对象是用来告诉ingress-controller该如何转发请求，比如哪些域名哪些path要转发到哪些服务等等。

#### pod的状态，pod处于pendding状态原因
    1. running     运行中     已绑定到节点上，并且pod中容器都被创建，且至少有一个已经在运行中，或者处于启动或者重启中
    2. pending     等待中     创建但未完成调度，可能正在写数据到etcd，下载镜像，调度中，启动容器
    3. field       异常停止    pod所有容器都已终止，并且至少有个容器因失败而停止
    4. susuccessed 正常停止    容器正常退出状态，并且不会重启，一般为job任务
    5. unkonwn     未知状态    api-service无法获取pod状态，通常是因为无法与所在节点kubelet通信

#### pod控制器说几个
     deployment
     service
     daemoset
     job
     cronjob
     statefulset  
     replication contorller
     replicatset
    
#### 无状态服务用deployment,而有状态服务用statefulset,他们的区别在哪里？ 
    1. deployment 无状态服务 

         pod间无顺序

         所有pod共享存储

         pod名字随机

         service都有classIP，可以负载均衡

    2. statefulset 有状态服务 
         部署，扩展，更新，删除都要有顺序

         每个pod都要有自己的存储，所以都要用voluem，为每个pod生成自己的存储

         pod名字是固定的

         service没有classIP，是headlessservice，无法负载均衡，返回都是pod名，

    3. 综上所述：
         如果是不需额外数据依赖或者状态维护的部署，或者replicas是1，优先考虑使用Deployment；

         如果单纯的要做数据持久化，防止pod宕掉重启数据丢失，那么使用pv/pvc就可以了；

         如果要打通app之间的通信，而又不需要对外暴露，使用headlessService即可；

         如果需要使用service的负载均衡，不要使用StatefulSet，尽量使用clusterIP类型，用serviceName做转发；

         如果是有多replicas，且需要挂载多个pv且每个pv的数据是不同的，因为pod和pv之间是 一 一对应的，如果某个pod挂掉再重启，还需要连接之前的pv，不能连到别的pv上，考虑使用StatefulSet

         能不用StatefulSet，就不要用

#### 版本回滚相关的命令？
    [root@master httpd-web]# kubectl apply -f httpd2-deploy1.yaml  --record
    #运行yaml文件，并记录版本信息；

    [root@master httpd-web]# kubectl rollout history deployment httpd-devploy1
    #查看该deployment的历史版本

    [root@master httpd-web]# kubectl rollout undo deployment httpd-devploy1 --to-revision=1
    #执行回滚操作，指定回滚到版本1

    #在yaml文件的spec字段中，可以写以下选项（用于限制最多记录多少个历史版本）：
    spec:
      revisionHistoryLimit: 5
    #这个字段通过 kubectl explain deploy.spec  命令找到revisionHistoryLimit   <integer>行获得

#### 常用的标签分类有哪些？
    标签分类是可以自定义的，但是为了能使他人可以达到一目了然的效果，一般会使用以下一些分类：

    版本类标签（release）：stable（稳定版）、canary（金丝雀版本，可以将其称之为测试版中的测试版）、beta（测试版）；

    环境类标签（environment）：dev（开发）、qa（测试）、production（生产）、op（运维）；

    应用类（app）：ui、as、pc、sc；

    架构类（tier）：frontend（前端）、backend（后端）、cache（缓存）；

    分区标签（partition）：customerA（客户A）、customerB（客户B）；

    品控级别（Track）：daily（每天）、weekly（每周）。

#### 创建一个pod的流程是什么？
    客户端提交Pod的配置信息（可以是yaml文件定义好的信息）到kube-apiserver；

    Apiserver收到指令后，通知给controller-manager创建一个资源对象；

    Controller-manager通过api-server将pod的配置信息存储到ETCD数据中心中；

    Kube-scheduler检测到pod信息会开始调度预选，会先过滤掉不符合Pod资源配置要求的节点，然后开始调度调优，主要是挑选出更适合运行pod的节点，然后将pod的资源配置单发送到node节点上的kubelet组件上。

    Kubelet根据scheduler发来的资源配置单运行pod，运行成功后，将pod的运行信息返回给scheduler，scheduler将返回的pod运行状况的信息存储到etcd数据中心。

#### 删除一个Pod会发生什么事情？
    答：Kube-apiserver会接受到用户的删除指令，默认有30秒时间等待优雅退出，超过30秒会被标记为死亡状态，此时Pod的状态Terminating，kubelet看到pod标记为Terminating就    开始了关闭Pod的工作；

    关闭流程如下：

    pod从service的endpoint列表中被移除；

    如果该pod定义了一个停止前的钩子，其会在pod内部被调用，停止钩子一般定义了如何优雅的结束进程；

    进程被发送TERM信号（kill -14）

    当超过优雅退出的时间后，Pod中的所有进程都会被发送SIGKILL信号（kill -9）。

#### k8s数据持久化的方式有哪些？
1）EmptyDir（空目录）：

    没有指定要挂载宿主机上的某个目录，直接由Pod内保部映射到宿主机上。类似于docker中的manager volume。

    主要使用场景：

    只需要临时将数据保存在磁盘上，比如在合并/排序算法中；
    
    作为两个容器的共享存储，使得第一个内容管理的容器可以将生成的数据存入其中，同时由同一个webserver容器对外提供这些页面。

    emptyDir的特性：

    同个pod里面的不同容器，共享同一个持久化目录，当pod节点删除时，volume的数据也会被删除。如果仅仅是容器被销毁，pod还在，则不会影响volume中的数据。

    总结来说：emptyDir的数据持久化的生命周期和使用的pod一致。一般是作为临时存储使用。

2）Hostpath：

    将宿主机上已存在的目录或文件挂载到容器内部。类似于docker中的bind mount挂载方式。

    这种数据持久化方式，运用场景不多，因为它增加了pod与节点之间的耦合。

    一般对于k8s集群本身的数据持久化和docker本身的数据持久化会使用这种方式，可以自行参考apiService的yaml文件，位于：/etc/kubernetes/main…目录下。

3）PersistentVolume（简称PV）：

    基于NFS服务的PV，也可以基于GFS的PV。它的作用是统一数据持久化目录，方便管理。

    在一个PV的yaml文件中，可以对其配置PV的大小，指定PV的访问模式：

    ReadWriteOnce：只能以读写的方式挂载到单个节点；

    ReadOnlyMany：能以只读的方式挂载到多个节点；

    ReadWriteMany：能以读写的方式挂载到多个节点。以及指定pv的回收策略：

    recycle：清除PV的数据，然后自动回收；

    Retain：需要手动回收；

    delete：删除云存储资源，云存储专用；

    PS：这里的回收策略指的是在PV被删除后，在这个PV下所存储的源文件是否删除）。

    若需使用PV，那么还有一个重要的概念：PVC，PVC是向PV申请应用所需的容量大小，K8s集群中可能会有多个PV，PVC和PV若要关联，其定义的访问模式必须一致。定义的storageClassName也必须一致，若群集中存在相同的（名字、访问模式都一致）两个PV，那么PVC会选择向它所需容量接近的PV去申请，或者随机申请。


#### nsenter命令使用场景
    比较最典型的用途是进入容器的网络命令空间，比如抓包。
    在做容器 debug 的时候，经常出现容器里没有某个命令的尴尬局面。比如ping，telnet等常用命令。如何才能快速debug网络问题，可以使用nsenter。
