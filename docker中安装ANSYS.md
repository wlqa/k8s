### docker中安装第三方软件方法（GUI）
- 拉取centos镜像安装“GNOME Desktop”，然后安装对应软件    ##测试不好使，有一些莫名的错误
- 通过本地已安装好的软件镜像打包成docker镜像
> 主要介绍第二种方法

### 本地软件打包成docker镜像
例如新安装的操作系统，并且已安装好相对应的软件

```
tar -cvpf /tmp/system.tar --directory=/ --exclude=proc --exclude=sys --exclude=dev --exclude=run --exclude=boot .

#/proc、/sys、/run、/dev这几个目录都是系统启动时自动生成的！依赖与系统内核！
#导入tar包生成docker镜像
docker import system.tar ansys-centos7

#查看镜像
docker images
REPOSITORY      TAG       IMAGE ID       CREATED             SIZE
ansys-centos7   18        2aa93b7916c5   About an hour ago   28.7GB

#运行容器
docker run -it --name ansys18 2aa93b7916c5  /bin/bash


```
> 目前存在的问题是，GUI的没有调出来，在解决中。。。。
